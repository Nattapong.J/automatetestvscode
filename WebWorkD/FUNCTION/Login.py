""" Import User Variable"""
import  function.variable as v


def login_web(driver):
    driver.maximize_window()
    
    # Production
    # driver.get("https://portal.kg-tech.io/auth/realms/master/protocol/openid-connect/auth?client_id=workd-web-application&redirect_uri=https%3A%2F%2Fworkd.kg-tech.io%2Fmain&state=6a7a11f2-90d4-4c95-9287-fcdadec72f51&response_mode=fragment&response_type=code&scope=openid&nonce=eb856c8d-c47c-4ddb-bec1-512befc03bd7")

    # # Staging
    driver.get("http://192.168.11.195:8100/auth/realms/master/protocol/openid-connect/auth?client_id=login-keycloak&redirect_uri=http%3A%2F%2F192.168.11.195%3A8201%2Fmain&state=c8d919d9-e039-49c8-ac2a-32ccf88e8669&response_mode=fragment&response_type=code&scope=openid&nonce=c523d047-7d32-45b3-8ae1-f28bc0126d3f")

    driver.find_element_by_xpath('//*[@id="username"]').send_keys(v.username)
    driver.find_element_by_xpath('//*[@id="password"]').send_keys(v.password)
    driver.implicitly_wait(10)
    # driver.get_screenshot_as_file("Login.png")
    driver.find_element_by_xpath('//*[@id="kc-form-login"]/div/div[3]/button').click()
    driver.implicitly_wait(10)
    print("Login Pass")